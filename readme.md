Breakout
###
Think "block breaker" but super simple. This is an example project for minimal use of the Xygine engine, a "2D Game Engine Framework built around SFML" by fallahn, docs available on GitHub. Xygine is the only top-level source depencency for the breakout game. The Xygine framework has the following dependencies:
* SFML 2.5.1
* enet
* tmxlite

Toolchain Setup
###

Before Building
######
* Install Visual Studio 2019
* Install hcc.exe
  * https://www.helixoft.com/vsdocman-faqs/where-can-i-download-chm-compiler-hhc-exe.html
* Install Doxygen
  * https://www.doxygen.nl/download.html
* Install cmake
  * https://cmake.org/download/


Building
######
After installing the prerequisites, run do_cmake.bat.

Only tested for Windows and Visual Studio 2019.

After building you will be prompted to open the Visual Studio solution located in build/breakout.sln.

Within Visual Studio, in the Solution Explorer (right side), right-click the project, breakout, and select Set As Startup Project.
