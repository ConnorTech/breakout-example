@ECHO OFF
setlocal

SET CMAKE_EXE="C:\Program Files\CMake\bin\cmake.exe"
SET GENERATOR="Visual Studio 16 2019"
SET GIT_EXE=git

SET "PROJECT_DIR=%~dp0"
SET "PROJECT_DIR=%PROJECT_DIR:\=/%"
SET "BUILD_DIR=%PROJECT_DIR%/build"

REM SET XYGINE_GIT_PATH=https://github.com/fallahn/xygine.git
SET XYGINE_GIT_PATH=https://github.com/murphycd/xygine.git
SET XYGINE_GIT_TAG=master
SET "XYGINE_SOURCE_PATH=%BUILD_DIR%/xygine/"
SET "XYGINE_INSTALL_PATH=%BUILD_DIR%/xyginext/"

:CREATE_TEMP_DIR
IF NOT EXIST "%BUILD_DIR%" MKDIR "%BUILD_DIR%"
cd "%BUILD_DIR%"

:FETCH_XYGINE
ECHO.
ECHO Looking for Xygine repo...
IF NOT EXIST "%XYGINE_SOURCE_PATH%\.git\" (
    ECHO "Cloning..."
    %GIT_EXE% clone %XYGINE_GIT_PATH% --branch %XYGINE_GIT_TAG% --depth 1 --shallow-submodules || goto error
) ELSE (
    cd "%XYGINE_SOURCE_PATH%"
    ECHO Found xygine...getting latest...
    %GIT_EXE% pull
)

:BUILD_XYGINE
ECHO.
ECHO Looking for Xygine output products...
IF NOT EXIST "%XYGINE_INSTALL_PATH%" (
    cd "%XYGINE_SOURCE_PATH%"
    set "XYGINE_BUILD_PATH=%XYGINE_SOURCE_PATH%/build"
    ECHO Generating Xygine build scripts...
    %CMAKE_EXE% -Wno-dev -G %GENERATOR% -DSFML_USE_STATIC_STD_LIBS:BOOL="1" -DBUILD_SHARED_LIBS:BOOL="0" -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;" -DCMAKE_MSVC_RUNTIME_LIBRARY="MultiThreaded$<$<CONFIG:Debug>:Debug>" -DSFML_BUILD_DOC:BOOL="1" -DCMAKE_INSTALL_PREFIX:PATH="%XYGINE_INSTALL_PATH%" -S "%XYGINE_SOURCE_PATH%" -B "%XYGINE_BUILD_PATH%" || goto error
    ECHO Build Xygine project...
    %CMAKE_EXE% --build "%XYGINE_BUILD_PATH%" --target install --config release || goto error
    %CMAKE_EXE% --build "%XYGINE_BUILD_PATH%" --target install --config debug || goto error
) ELSE (
ECHO Found %XYGINE_INSTALL_PATH%
)

:GENERATE
ECHO.
ECHO Generating project using CMAKE ...
cd "%PROJECT_DIR%"
ECHO %cd%
%CMAKE_EXE% -G %GENERATOR% -DCMAKE_INSTALL_PREFIX:PATH=%PROJECT_DIR% -DCMAKE_BUILD_TYPE:STRING="Debug" -DSFML_DOC_DIR:PATH="%XYGINE_INSTALL_PATH%/doc" -DXYGINEXT_DIR:PATH="%XYGINE_INSTALL_PATH%/lib/cmake/xyginext" -DSFML_STATIC_LIBRARIES:BOOL="1" -DBUILD_SHARED_LIBS:BOOL="0" -DSFML_DIR:PATH="%XYGINE_INSTALL_PATH%/lib/cmake/SFML" -DXY_RUNTIME_FILES="%XYGINE_INSTALL_PATH%/bin" -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;" -S %PROJECT_DIR% -B %BUILD_DIR% || goto error

:BUILD
ECHO.
ECHO Building project using %GENERATOR% ...
%CMAKE_EXE% --build %BUILD_DIR% --target install --config release || goto error
%CMAKE_EXE% --build %BUILD_DIR% --target install --config debug || goto error

:PROMPT_OPEN
SET /P PROMPTTEXT=Open the generated project? (Y/[N])?
IF /I "%PROMPTTEXT%" NEQ "Y" GOTO END
%CMAKE_EXE% --open %BUILD_DIR%
GOTO END

:error 
pause
exit /b %errorlevel%

:END
endlocal
:EOF
